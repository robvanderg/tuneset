import os
import myutils

#for setup in ['HR', 'LR']:
for setup in ['LR']:
    for dataset in myutils.relTreebanks:
        bestOld = dataset + '.' + myutils.getBestUU(dataset, True, setup)
        if bestOld == '':
            continue
        #print(bestOld)
    
        oldModel = 'models/' + bestOld
        newModel = 'models/uuparser.test.' + dataset + '.' + setup
    
        # Old:
        test = '../newsplits-v2.7/' + dataset + '/test.conllu'
        cmd = 'python3 uuparser/parser.py --predict --modeldir '
        cmd += oldModel.replace('uuparser/', '') + ' --testfile '
        cmd += test + ' --outdir ../preds/test.old.' + dataset + '.' + setup + '/'
        #cmd += ' --graph-based' # hardCoded?!
        print(cmd)
    
        # New:
        test = '../newsplits-v2.7/' + dataset + '/test.conllu'
        cmd = 'python3 uuparser/parser.py --predict --modeldir '
        cmd += newModel.replace('uuparser/', '') + ' --testfile '
        cmd += test + ' --outdir ../preds/test.new.' + dataset + '.' + setup + '/'
        #cmd += ' --graph-based' # hardCoded?!
        print(cmd)
    

