import os
import myutils

def getTrainDevTest(path):
    train = ''
    dev = ''
    test = ''
    tune = ''
    for conlFile in os.listdir(path):
        if conlFile.endswith('conllu'):
            if 'train' in conlFile:
                train = path + '/' + conlFile
            if 'dev' in conlFile:
                dev = path + '/' + conlFile
            if 'test' in conlFile:
                test = path + '/' + conlFile
    return train, dev, test

def getSize(path):
    if os.path.isfile(path):
        return sum([int(len(line) < 3) for line in open(path)])
    return 0

def readConll(path):
    if not os.path.isfile(path):
        return []
    data = [[]]
    for line in open(path):
        if len(line) < 3:
            data.append([])
        else:
            data[-1].append(line)
    return data[:-1] # remove last empty one

def resize(data, beg, end, newPath):
    newFile = open(newPath, 'w')
    for conlSent in data[beg:end]:
        newFile.write(''.join(conlSent) + '\n')
    newFile.close()


#consider = []
#for treebankDir in relTreebanks:
#    trainPath, devPath, testPath = getTrainDevTest(udDir + treebankDir)
#    devSize = getSize(devPath)
#    trainSize = getSize(trainPath)
#    
#    if trainSize + devSize > 5000:
#        consider.append(treebankDir)

for setup in ['LR' , 'HR']:
    tgtDir = 'newsplits-v2.7-' + setup + '/'
    if not os.path.isdir(tgtDir):
        os.mkdir(tgtDir)

    for treebankDir in myutils.relTreebanks:
        tgtTreebankDir = tgtDir + treebankDir + '/'
        if not os.path.isdir(tgtTreebankDir):
            os.mkdir(tgtTreebankDir)
        trainPath, devPath, testPath = getTrainDevTest(myutils.udDir + treebankDir)
        trainData = readConll(trainPath)
        devData = readConll(devPath)
        fullData = trainData + devData
        numSents = len(fullData)
        print(numSents)
        if setup == 'HR':
            resize(fullData, numSents-1000, numSents, tgtTreebankDir + 'test.conllu')
            resize(fullData, numSents-2000, numSents-1000, tgtTreebankDir + 'dev.conllu')
            resize(fullData, numSents-3000, numSents-2000, tgtTreebankDir + 'tune.conllu')
            resize(fullData, 0, min(numSents-3000, 20000), tgtTreebankDir + 'train.conllu')
            resize(fullData, 0, min(numSents-2000, 20000), tgtTreebankDir + 'trainPlusTune.conllu')
        if setup == 'LR':
            resize(fullData, numSents-1000, numSents, tgtTreebankDir + 'test.conllu')
            resize(fullData, numSents-1250, numSents-1000, tgtTreebankDir + 'dev.conllu')
            resize(fullData, numSents-1500, numSents-1250, tgtTreebankDir + 'tune.conllu')
            resize(fullData, 0, min(numSents-1500, 2000), tgtTreebankDir + 'train.conllu')
            resize(fullData, 0, min(numSents-1250, 2250), tgtTreebankDir + 'trainPlusTune.conllu')


