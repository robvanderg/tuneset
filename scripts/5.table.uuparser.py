import myutils
import os
import conll
import conll18_ud_eval
import bootstrapSign
from deepsig import bootstrap_test, permutation_test

def getName(fullName):
    for conlFile in os.listdir('ud-treebanks-v2.7/' + fullName):
        if conlFile.endswith('.conllu'):
            return conlFile.split('-')[0]
    return fullName

def getScores(gold, pred):
    results = []
    for goldSent, predSent in zip(gold, pred):
        results.append(conll18_ud_eval.evaluate(goldSent, predSent)['LAS'].f1)
    return results

olds = []
news = []
for dataset in myutils.relTreebanks:
    bestOld = myutils.getBestUU(dataset, True).split('.')[:-1]
    bestNew = myutils.getBestUU(dataset, False).split('.')
    oldTestPath = 'preds/test.old.' + dataset + '/out.conllu.txt'
    newTestPath = 'preds/test.new.' + dataset + '/out.conllu.txt'
    oldScore = myutils.getScoreUU(oldTestPath)
    newScore = myutils.getScoreUU(newTestPath)

    oldDevPath = 'preds/' + dataset + '.' + '.'.join(bestOld) + '.oldSetup/out.conllu.txt'
    newDevPath = 'preds/' + dataset + '.' + '.'.join(bestNew) + '/out.conllu.txt'
    oldDevScore = myutils.getScoreUU(oldDevPath)
    newDevScore = myutils.getScoreUU(newDevPath)

    olds.append(oldScore-oldDevScore)
    news.append(newScore-newDevScore)
    name = getName(dataset)
    print(' & '.join([name, '{:.2f}'.format(oldScore-oldDevScore), '{:.2f}'.format(newScore-newDevScore)]) + ' \\\\')

import bootstrapSign
p = bootstrapSign.getP([olds, news])
print(p)

