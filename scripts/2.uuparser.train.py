import os
import random
import myutils
random.seed(8446)

cmds = []
for setup in ['LR', 'HR']:
    for newSetup in [False, True]:
        for dataset in myutils.relTreebanks:
            train = '../newsplits-v2.7-' + setup + '/' + dataset + '/train.conllu'
            tune = '../newsplits-v2.7-' + setup + '/' + dataset + '/tune.conllu'
            if newSetup == False:
                train = '../newsplits-v2.7-' + setup + '/' + dataset + '/trainPlusTune.conllu'
                tune = '../newsplits-v2.7-' + setup +'/' + dataset + '/dev.conllu'
            for graph in [False, True]:
                for learnRate in [1e-2, 1e-3, 1e-4]:
                    for wordSize in [50,100,200]:
                        for charSize in [100,500]:
                            for numBILSTM in [1,2]:
                                cmd = 'python3 uuparser/parser.py --trainfile ' + train
                                cmd += ' --devfile ' + tune
                                cmd += ' --learning-rate ' + str(learnRate)
                                cmd += ' --word-emb-size ' + str(wordSize)
                                cmd += ' --char-emb-size ' + str(charSize)
                                cmd += ' --no-bilstms ' + str(numBILSTM)
                                if graph:
                                    cmd += ' --graph-based'
                                name = '.'.join([dataset, str(int(graph)), str(learnRate), str(wordSize), str(charSize), str(numBILSTM), setup])
                                if not newSetup:
                                    name += '.oldSetup'
                                cmd += ' --outdir ' + ' models/' + name
                                if not os.path.isfile('uuparser/models/' + name + '/barchybrid.model'):
                                    cmds.append(cmd)
    
# shuffle to make subsplits more likely to take evenly long
random.shuffle(list(set(cmds)))
for cmd in cmds:
    print(cmd)


