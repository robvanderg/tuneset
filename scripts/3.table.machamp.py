import myutils
import os
import ast
import conll
import conll18_ud_eval
import bootstrapSign
from deepsig import bootstrap_test, permutation_test

def getName(fullName):
    for conlFile in os.listdir('ud-treebanks-v2.7/' + fullName):
        if conlFile.endswith('.conllu'):
            return conlFile.split('-')[0]
    return fullName

def getScores(gold, pred):
    results = []
    for goldSent, predSent in zip(gold, pred):
        results.append(conll18_ud_eval.evaluate(goldSent, predSent)['LAS'].f1)
    return results

#for setup in ['LR', 'HR']:
for setup in ['LR']:
    for dataset in myutils.relTreebanks:
        bestOld = myutils.getBestMachamp(dataset, True, setup).split('.')[1:-1]
        bestNew = myutils.getBestMachamp(dataset, False, setup).split('.')[1:]
        diffCount = 0
        for oldItem, newItem in zip(bestOld, bestNew):
            if oldItem != newItem:
                diffCount += 1
        oldTestPath = 'preds/' + dataset + '.test.' + setup + '.oldSetup.eval'
        newTestPath = 'preds/' + dataset + '.test.' + setup + '.eval'
        oldScore = ast.literal_eval('\n'.join(open(oldTestPath).readlines()))['.run/dependency/las'] * 100
        newScore = ast.literal_eval('\n'.join(open(newTestPath).readlines()))['.run/dependency/las'] * 100
    
        oldSents = conll.toConllFile('preds/' + dataset + '.test.' + setup + '.oldSetup')
        newSents = conll.toConllFile('preds/' + dataset + '.test.' + setup)
        goldSents = conll.toConllFile('newsplits-v2.7-' + setup + '/' + dataset + '/test.conllu')
        oldScores = getScores(goldSents, oldSents)
        newScores = getScores(goldSents, newSents)
        # test with 2 packages to be 100% sure
        p = bootstrapSign.getP([oldScores, newScores])
        if sum(oldScores) > sum(newScores):
            otherP = bootstrap_test(oldScores, newScores, num_samples=10000)
        else:
            otherP = bootstrap_test(newScores, oldScores, num_samples=10000)
        smallestP = min(otherP, p)
        if smallestP < 0.05:
            print("SIGN")
            if smallestP < 0.05/len(myutils.relTreebanks):
                print("Bonferroni!")
        name = getName(dataset)
        print(' & '.join([name, str(diffCount) + '/' + str(len(bestOld)), '{:.2f}'.format(oldScore), '{:.2f}'.format(newScore)]) + ' \\\\')
 
    

