import random
import os
import myutils
random.seed(8446)

for setup in ['LR', 'HR']:
    for newSetup in [False, True]:
        for dataset in myutils.relTreebanks:
            for dropout in [.4, .2, .3]:
                for cut_frac in [.1,.2, .3]:
                    for decay in [.35, .38, .5]:
                        lang = dataset.split('_')[1].split('-')[0]
                        name = '.'.join([str(x).replace('.','') for x in [dropout, cut_frac, decay, setup]])
                        if newSetup == False:
                            name += '.oldSetup'
                        model = myutils.getModel(dataset + '.' + name)
                        if model == '':
                            print('MODEL NOT FOUND', dataset + '.' + name)
                        else:
                            goldFile = '../newsplits-v2.7-' + setup + '/' + dataset + '/dev.conllu'
                            outFile = '../preds/' + dataset + '.' + name 
                            cmd = 'python3 predict.py ' + model[4:] + ' ' + goldFile + ' ' + outFile + ' > ' + outFile + '.eval'
                            if not os.path.isfile(outFile[3:]):
                                print(cmd)
    


