import conll18_ud_eval
import sys

class conllFile:
    def __init__(self, data):
        self.data = data
        self.idx = -1
    def readline(self):
        self.idx +=1
        if self.idx != len(self.data):
            return self.data[self.idx]

def toConllFile(path):
    data = []
    curSent = []
    for line in open(path):
        curSent.append(line)
        if len(line) < 3:
            data.append(conll18_ud_eval.load_conllu(conllFile(curSent)))
            curSent = []
    return data

#data1 = toConllFile(sys.argv[1])
#data2 = toConllFile(sys.argv[2])
#for sent1, sent2 in zip(data1, data2):
#    print(conll18_ud_eval.evaluate(sent1, sent2)['LAS'].f1)

