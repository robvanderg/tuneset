import os
import ast

udDir = 'ud-treebanks-v2.7/'
relTreebanks = ['UD_Ancient_Greek-PROIEL', 'UD_Arabic-PADT', 'UD_English-EWT', 'UD_Finnish-TDT', 'UD_Chinese-GSD', 'UD_Hebrew-HTB', 'UD_Korean-GSD', 'UD_Russian-GSD', 'UD_Swedish-Talbanken']

def getModel(name):
    modelDir = 'mtp/logs/'
    nameDir = modelDir + name + '/'
    if os.path.isdir(nameDir):
        for modelDir in reversed(os.listdir(nameDir)):
            modelPath = nameDir + modelDir + '/model.tar.gz'
            if os.path.isfile(modelPath):
                return modelPath
    return ''

def getBestMachamp(dataset, old, setup):
    highest = 0.0
    highestName = ''
    for dropout in [.4, .2, .3]:
        for cut_frac in [.1,.2, .3]:
            for decay in [.35, .38, .5]:
                name = '.'.join([str(x).replace('.','') for x in [dataset, dropout, cut_frac, decay, setup]])
                if old:
                    name += '.oldSetup'
                path = 'preds/' + name + '.eval'
                # not all can be found, so we can not say for sure whats the
                # the best, return empty
                if not os.path.isfile(path):
                    print('ERROR, not found: ' + path)
                    return ''
                score = ast.literal_eval('\n'.join(open(path).readlines()))['.run/dependency/las']
                if score > highest:
                    highest = score
                    highestName = name
    return highestName

def getScoreUU(path):
    if os.path.isfile(path):
        for line in open(path):
            if line.startswith('LAS'):
                return float(line.strip().split()[-1])
    return -99

def getBestUU(dataset, old, setup):
    highestScore = 0.0
    highestName = ''
    for graph in [False, True]:
        for learnRate in [1e-2, 1e-3, 1e-4]:
            for wordSize in [50,100,200]:
                for charSize in [100,500]:
                    for numBILSTM in [1,2]:
                        name = '.'.join([str(int(graph)), str(learnRate), str(wordSize), str(charSize), str(numBILSTM), setup])
                        if old:
                            name += '.oldSetup'
                        score = getScoreUU('preds/' + dataset + '.' + name + '/out.conllu.txt')
                        if score == -99:
                            print('ERROR preds/' + dataset + '.' + name + '/out.conllu.tx not found')
                        if score > highestScore:
                            highestScore = score
                            highestName = name
    return highestName

