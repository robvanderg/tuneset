import os
import myutils

def fixModel(path):
    if os.path.isdir(path):
        if os.path.isfile(path + '/barchybrid.model'):
            return 
        else:
            for i in reversed(range(100)):
                if os.path.isfile(path + '/barchybrid.model' + str(i)):
                    os.system('cp ' + path + '/barchybrid.model' + str(i) + ' ' + path + '/barchybrid.model')
                    break
    return

for setup in ['LR', 'HR']:
    for newSetup in [False, True]:
        for dataset in myutils.relTreebanks:
            dev = '../newsplits-v2.7-' + setup + '/' + dataset + '/dev.conllu'
            for graph in [False, True]:
                for learnRate in [1e-2, 1e-3, 1e-4]:
                    for wordSize in [50,100,200]:
                        for charSize in [100,500]:
                            for numBILSTM in [1,2]:
                                name = '.'.join([dataset, str(int(graph)), str(learnRate), str(wordSize), str(charSize), str(numBILSTM), setup])
                                if not newSetup:
                                    name += '.oldSetup'
                                cmd = 'python3 uuparser/parser.py --predict --modeldir '
                                cmd += 'models/' + name + '/ --testfile ' + dev  
                                cmd += ' --outdir ../preds/' + name + '/'
                                if graph:
                                    cmd += ' --graph-based'
                                if os.path.isfile('uuparser/models/' + name + '/barchybrid.model'):
                                    if not os.path.isfile('preds/' + name + '/out.conllu.txt'):
                                        print(cmd)
                                elif os.path.isfile('uuparser/models/' + name + '/barchybrid.model1'):
                                    # fix model
                                    fixModel('uuparser/models/' + name)
                                    if not os.path.isfile('preds/' + name + '/out.conllu.txt'):
                                        print(cmd)
    
    
