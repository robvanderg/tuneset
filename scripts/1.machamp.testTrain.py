import myutils

for setup in ['HR', 'LR']:
    for dataset in myutils.relTreebanks:
        model = myutils.getBestMachamp(dataset, False, setup)
        if model == '':
            continue
        bestNew = model.split('.')[1:]

        bestNewName = '.'.join(bestNew)

        paramsFile = '../configs/params.' + bestNewName + '.json'
        datasetFile = '../configs/' + dataset + '.' + setup + '.oldSetup.json'
        if myutils.getModel('test.' + dataset + '.' + setup) == '':
            cmd = 'python3 train.py --dataset_config ' + datasetFile + ' --parameters_config ' + paramsFile + ' --name ' + 'test.' + dataset + '.' + setup
            print(cmd)

