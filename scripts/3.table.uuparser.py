import myutils
import os
import conll
import conll18_ud_eval
import bootstrapSign
from deepsig import bootstrap_test, permutation_test

def getName(fullName):
    for conlFile in os.listdir('ud-treebanks-v2.7/' + fullName):
        if conlFile.endswith('.conllu'):
            return conlFile.split('-')[0]
    return fullName

def getScores(gold, pred):
    results = []
    for goldSent, predSent in zip(gold, pred):
        results.append(conll18_ud_eval.evaluate(goldSent, predSent)['LAS'].f1)
    return results

#for setup in ['HR', 'LR']:
for setup in ['LR']:
    for dataset in myutils.relTreebanks:
        bestOld = myutils.getBestUU(dataset, True, setup).split('.')[:-1]
        bestNew = myutils.getBestUU(dataset, False, setup).split('.')
        diffCount = 0
        for oldItem, newItem in zip(bestOld, bestNew):
            if oldItem != newItem:
                diffCount += 1
        oldTestPath = 'preds/test.old.' + dataset + '.' + setup + '/out.conllu.txt'
        newTestPath = 'preds/test.new.' + dataset + '.' + setup + '/out.conllu.txt'
        if not os.path.isfile(oldTestPath) or not os.path.isfile(newTestPath):
            continue
        oldScore = myutils.getScoreUU(oldTestPath)
        newScore = myutils.getScoreUU(newTestPath)
    
        oldSents = conll.toConllFile('preds/test.old.' + dataset + '.' + setup + '/out.conllu')
        newSents = conll.toConllFile('preds/test.new.' + dataset + '.' + setup + '/out.conllu')
        goldSents = conll.toConllFile('newsplits-v2.7-' + setup + '/' + dataset + '/test.conllu')
        oldScores = getScores(goldSents, oldSents)
        newScores = getScores(goldSents, newSents)
        # test with 2 packages to be 100% sure
        p = bootstrapSign.getP([oldScores, newScores])
        if sum(oldScores) > sum(newScores):
            otherP = bootstrap_test(oldScores, newScores, num_samples=10000)
        else:
            otherP = bootstrap_test(newScores, oldScores, num_samples=10000)
        smallestP = min(otherP, p)
        if smallestP < 0.05:
            print("SIGN")
            if smallestP < 0.05/len(myutils.relTreebanks):
                print("Bonferroni!")
        name = getName(dataset)
        print(' & '.join([name, str(diffCount) + '/' + str(len(bestOld)+1), '{:.2f}'.format(oldScore), '{:.2f}'.format(newScore)]) + ' \\\\')
    


