import myutils
import os
import conll
import conll18_ud_eval
import bootstrapSign
from deepsig import bootstrap_test, permutation_test

def getName(fullName):
    for conlFile in os.listdir('ud-treebanks-v2.7/' + fullName):
        if conlFile.endswith('.conllu'):
            return conlFile.split('-')[0]
    return fullName

print('\\toprule')
print('Data & Graph & LR & WordS. & CharS. & \#-layers \\\\')
print('\\midrule')
for dataset in myutils.relTreebanks:
    bestOld = myutils.getBestUU(dataset, True).split('.')[:-1]
    bestNew = myutils.getBestUU(dataset, False).split('.')
    bestOld = [bestOld[0]] + bestOld[2:]
    bestNew = [bestNew[0]] + bestNew[2:]
    bestOld[1] = '0.' + str(bestOld[1])
    bestNew[1] = '0.' + str(bestNew[1])
    print('\\multicolumn{5}{l}{' + dataset.replace('_','\\_') + '} \\\\')
    print(' & '.join(['-Tune'] + bestOld) + ' \\\\')
    print(' & '.join(['+Tune'] + bestNew) + ' \\\\')
print('\\bottomrule')

print()
print('\\toprule')
print('Data & LR & dropout & cut\\_frac & decay \\\\')
print('\\midrule')
for dataset in myutils.relTreebanks:
    bestOld = ['0.0001'] + [x[0] + '.' + x[1:] for x in myutils.getBestMachamp(dataset, True).split('.')[1:-1]]
    bestNew = ['0.0001'] + [x[0] + '.' + x[1:] for x in myutils.getBestMachamp(dataset, False).split('.')[1:]]
    print('\\multicolumn{5}{l}{' + dataset.replace('_', '\\_') + '} \\\\')
    print(' & '.join(['-Tune'] + bestOld) + ' \\\\')
    print(' & '.join(['+Tune'] + bestNew) + ' \\\\')
print('\\bottomrule')

