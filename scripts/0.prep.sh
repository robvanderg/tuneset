# get and clean data
wget https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3424/ud-treebanks-v2.7.tgz
tar -zxvf ud-treebanks-v2.7.tgz
python3 scripts/0.resplit.py
python3 mtp/scripts/misc/cleanconl.py newsplits-v2.7/*/*conllu

# get machamp
git clone https://bitbucket.org/ahmetustunn/mtp.git
cd mtp
git reset --hard fae2b7fdc1850de42b8c85c006c1689db93a0f8f
pip3 install -r requirements.txt
cd ../

# get uuparser
git clone https://github.com/UppsalaNLP/uuparser.git
cd uuparser
git reset --hard c0d8a8210c1032272dfad9250a765f09e128976f
pip3 install -r requirements.txt
pip3 install loguru
# patch
sed -i "s;from uuparser\.;from ;g" uuparser/*py
sed -i "s;from uuparser ;;g" uuparser/*py
mkdir models
cd ..


