import myutils
import os
import ast
import conll
import conll18_ud_eval
import bootstrapSign
from deepsig import bootstrap_test, permutation_test

def getName(fullName):
    for conlFile in os.listdir('ud-treebanks-v2.7/' + fullName):
        if conlFile.endswith('.conllu'):
            return conlFile.split('-')[0]
    return fullName

def getScores(gold, pred):
    results = []
    for goldSent, predSent in zip(gold, pred):
        results.append(conll18_ud_eval.evaluate(goldSent, predSent)['LAS'].f1)
    return results
olds = []
news = []
for dataset in myutils.relTreebanks:
    bestOld = myutils.getBestMachamp(dataset, True).split('.')[1:-1]
    bestNew = myutils.getBestMachamp(dataset, False).split('.')[1:]
    oldTestPath = 'preds/' + dataset + '.test.oldSetup.eval'
    newTestPath = 'preds/' + dataset + '.test.eval'
    oldScore = ast.literal_eval('\n'.join(open(oldTestPath).readlines()))['.run/dependency/las'] * 100
    newScore = ast.literal_eval('\n'.join(open(newTestPath).readlines()))['.run/dependency/las'] * 100
    oldDevPath = 'preds/' + dataset + '.' + '.'.join(bestOld) + '.oldSetup.eval'
    newDevPath = 'preds/' + dataset + '.' + '.'.join(bestOld) + '.eval'
    oldDevScore = ast.literal_eval('\n'.join(open(oldDevPath).readlines()))['.run/dependency/las'] * 100
    newDevScore = ast.literal_eval('\n'.join(open(newDevPath).readlines()))['.run/dependency/las'] * 100
    olds.append(oldScore-oldDevScore)
    news.append(newScore-newDevScore)
    name = getName(dataset)
    print(' & '.join([name, '{:.2f}'.format(oldScore-oldDevScore), '{:.2f}'.format(newScore-newDevScore)]) + ' \\\\')

import bootstrapSign
p = bootstrapSign.getP([olds, news])
print(p)

    

