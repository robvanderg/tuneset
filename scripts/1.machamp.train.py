from allennlp.common import Params
import random
import myutils
import os
random.seed(8446)

def getSize(path):
    return sum([1 if len(line) < 2 else 0 for line in open(path).readlines()])

if not os.path.isdir('configs'):
    os.mkdir('configs')

cmds = []
for setup in ['LR' ,'HR']:
    for newSetup in [False, True]:
        for dataset in myutils.relTreebanks:
            dataconfig_path = 'configs/' + dataset + '.' + setup + ('' if newSetup else '.oldSetup') + '.json'
            dataset_config = Params.from_file('mtp/configs/ewt.json')
            dataset_config['UD_EWT']['train_data_path'] = '../newsplits-v2.7-' + setup +'/' + dataset + '/train.conllu'
            dataset_config['UD_EWT']['validation_data_path'] = '../newsplits-v2.7-' + setup + '/' + dataset + '/tune.conllu'
            if newSetup == False:
                dataset_config['UD_EWT']['train_data_path'] = '../newsplits-v2.7-' + setup + '/' + dataset + '/trainPlusTune.conllu'
                dataset_config['UD_EWT']['validation_data_path'] = '../newsplits-v2.7-' + setup + '/' + dataset + '/dev.conllu'
            dataset_config.to_file(dataconfig_path)
            dataconfig_path = '../' + dataconfig_path
            for dropout in [.4, .2, .3]:
                for cut_frac in [.1,.2, .3]:
                    for decay in [.35, .38, .5]:
                        base = Params.from_file('mtp/configs/params.json')
                        base['trainer']['learning_rate_scheduler']['decay_factor'] = decay
                        base['trainer']['learning_rate_scheduler']['cut_frac'] = cut_frac
                        base['model']['dropout'] = dropout
                        name = '.'.join([str(x).replace('.','') for x in [dropout, cut_frac, decay, setup]])
                        if newSetup == False:
                            name += '.oldSetup'
                        name += '.' + setup
                        model = myutils.getModel(dataset + '.' + name)
                        if model == '':
                            base.to_file('configs/params.' + name + '.json')
                            cmd = 'python3 train.py --name ' + dataset + '.' + name 
                            cmd += ' --dataset_config ' + dataconfig_path
                            cmd += ' --parameters_config ../configs/params.' + name + '.json'
                            cmds.append(cmd)

# shuffle to make subsplits more likely to take evenly long
random.shuffle(cmds)
for cmd in cmds:
    print(cmd)


