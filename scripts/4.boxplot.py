import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
plt.style.use('scripts/nice.mplstyle')


def readYear(path):
    allnums = []
    less5 = more4 = analysDev = analysTest = devYes = devNo = 0
    for line in open(path):
        tok = line.split(' ')
        if len(tok) > 3 and 'na' not in tok[1].lower():
            numtests = tok[1].replace('test','')
            if '-' in numtests:
                numtests = sum([int(x) for x in numtests.split('-')])/2
            else:
                numtests = int(numtests)
            if numtests < 5:
                less5 += 1
            else:
                more4 += 1
            allnums.append(numtests)

            analysis = tok[3]
            if analysis == 'analysis-train' or analysis == 'analysis-dev':
                analysDev += 1
            elif analysis == 'analysis-test':
                analysTest += 1
            #else:
            #    print(analysis)

            hasDev = tok[4].strip()
            if hasDev == 'dev+':
                devYes += 1
            elif hasDev == 'dev-':
                devNo += 1
            else:
                print(hasDev)
    return allnums #[less5, more4, analysDev, analysTest, devYes, devNo]

data10 = readYear('annotation/annotation-2010.txt')
data20 = readYear('annotation/annotation-2020.txt')
print(data10)
print(data20)

from scipy import stats
print(stats.ttest_ind(data10, data20))


fig, ax = plt.subplots(figsize=(3,3), dpi=300)
boxes = ax.boxplot([data10, data20], widths=.5)
#for box in boxes['boxes']:
#    box.set(linewidth=2)
ax.set_ylim((0,29))
ax.set_xticklabels(['2010', '2020'])

fig.savefig('boxplot.pdf', bbox_inches='tight')

