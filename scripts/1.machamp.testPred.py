import myutils

#for setup in ['HR', 'LR']:
for setup in ['LR']:
    for dataset in myutils.relTreebanks:
        bestModel = myutils.getBestMachamp(dataset, True, setup)
        bestOld = bestModel.split('.')[1:-1]
        bestOldName = '.'.join([dataset] + bestOld + ['oldSetup'])
        
        bestOldModel = myutils.getModel(bestOldName)
        testPath = '../newsplits-v2.7-' + setup + '/' + dataset + '/test.conllu'
        if bestOldModel != '':
            outFile = '../preds/' + dataset + '.test.' + setup + '.oldSetup'
            cmd = 'python3 predict.py ' + bestOldModel[4:] + ' ' + testPath + ' ' + outFile + ' > ' + outFile + '.eval'
            print(cmd)
        else:
            print('model ' + bestOldName + ' not found')
        
        bestNewModel = myutils.getModel('test.' + dataset + '.' + setup)
        if bestNewModel != '':
            outFile = '../preds/' + dataset + '.test.' + setup
            cmd = 'python3 predict.py ' + bestNewModel[4:] + ' ' + testPath + ' ' + outFile + ' > ' + outFile + '.eval'
            print(cmd)
        else:
            print('model test.' + dataset + '.' + setup + ' not found')

