import os

def getLAS(path):
    for line in open(path):
        if line.startswith('LAS'):
            return float(line.split('|')[3].replace(' ', ''))
    return 0.0

rootDir = 'uuparser/models/'
for modelDir in os.listdir(rootDir):
    if os.path.isfile(rootDir + modelDir + '/barchybrid.model'):
        continue
    highestScore = 0.0
    highestEpoch = 0
    for epoch in range(30):
        path = rootDir + modelDir + '/dev_epoch_' + str(epoch) + '.conllu.txt'
        if os.path.isfile(path):
            las = getLAS(path)
            if las > highestScore:
                highestScore = las
                highestEpoch = epoch
    if highestEpoch != 0:
        cmd = 'mv ' + rootDir + modelDir + '/barchybrid.model' + str(highestEpoch) + ' ' + rootDir + modelDir + '/barchybrid.model'
        print(cmd)


