import os
import myutils


for setup in ['LR', 'HR']:
    for dataset in myutils.relTreebanks:
        bestNew = myutils.getBestUU(dataset, False, setup).split('.')
        if bestNew == ['']:
            continue
        train = '../newsplits-v2.7-' + setup +  '/' + dataset + '/trainPlusTune.conllu'
        dev = '../newsplits-v2.7-' + setup + '/' + dataset + '/dev.conllu'
        cmd = 'python3 uuparser/parser.py --trainfile ' + train
        cmd += ' --devfile ' + dev
        cmd += ' --learning-rate 0.' + str(bestNew[2])
        cmd += ' --word-emb-size ' + str(bestNew[3])
        cmd += ' --char-emb-size ' + str(bestNew[4])
        cmd += ' --no-bilstms ' + str(bestNew[5])
        if int(bestNew[0]) == 1:
            cmd += ' --graph-based'
        cmd += ' --outdir models/uuparser.test.' + dataset + '.' + setup + '/'
        if not os.path.isfile(' --outdir models/uuparser.test.' + dataset + '.' + setup + '/barchybrid.model'):
            print(cmd)
    

