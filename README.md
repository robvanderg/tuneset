# We Need to Talk About train-dev-test Splits

## How to reproduce the experiments
You can see all the commands to train/evaluate the models in
`scripts/runAll.sh`. After all these commands are ran, the output of the models
should be in the `preds/` folder. After publication, we will also publish the
predictions, however it was too large to include.

The annotations with the number of evaluations on the test set per paper will 
not be publicly released, as it is not our goal to ``name and shame''.


## Update 26-10-2021
In the period between acceptance and camera ready I was rather busy, as a result I did
not manage to add the experiments I wanted to. This includes a reproduction of the
experiments on smaller data (25% of the original) suggested by a reviewer. I am still
planning to run these experiments, and will push the results to this repo.

This also led to a mistake in the camera ready: I actually used ud 2.7 instead 2.8.

Since the original splits for the UD data were designed, we realized in other projects
that the proposed splits are impractical. We have designed new splits, which can be 
obtained with scripts/9.UD-resplit.py. The main change is that in the new split, smaller
datasets can also be used (dev/tune size is a function of dataset size: 1/4th each).

